using Gtk 4.0;
using Adw 1;

template PaperNotebooksBar : Box {

  hexpand: false;
  vexpand: true;
  orientation: vertical;

  styles ["notebooks-bar"]

  Adw.HeaderBar header_bar {

    show-start-title-buttons: bind PaperNotebooksBar.paned;
    show-end-title-buttons: false;

    [title]
    Box {
      .PaperAppMenu {
        visible: bind PaperNotebooksBar.paned inverted;
      }

      Adw.WindowTitle window_title {
        visible: bind PaperNotebooksBar.paned;
      }
    }

    [end]
    .PaperAppMenu {
      visible: bind PaperNotebooksBar.paned;
    }
  }

  ScrolledWindow scrolled_window {

    hexpand: false;
    vexpand: true;
    hscrollbar-policy: never;
    vscrollbar-policy: external;

    Box {
      orientation: vertical;

      Revealer all_button_revealer {
        reveal-child: false;
        transition-type: slide_down;

        Box {

          ToggleButton all_button {

            visible: bind PaperNotebooksBar.paned inverted;
            icon-name: "view-list-symbolic";
            hexpand: true;
            vexpand: false;

            styles ["flat", "all-button"]
          }

          ToggleButton {

            visible: bind PaperNotebooksBar.paned;
            hexpand: true;
            vexpand: false;
            active: bind all_button.active bidirectional;
            sensitive: bind all_button.sensitive bidirectional;

            styles ["flat", "all-button"]

            Box {

              hexpand: true;
              orientation: horizontal;

              Image {
                icon-name: "view-list-symbolic";
              }

              Label {
                ellipsize: end;
                halign: start;
                label: _("All Notes");
              }
            }
          }
        }
      }

      ListView list {
        hexpand: false;
        vexpand: true;
      }
    }
  }

  ToggleButton trash_button {

    visible: bind PaperNotebooksBar.paned inverted;
    icon-name: "user-trash-symbolic";
    hexpand: false;
    vexpand: false;

    styles ["flat", "trash-button"]
  }

  ToggleButton {

    visible: bind PaperNotebooksBar.paned;
    hexpand: false;
    vexpand: false;
    active: bind trash_button.active bidirectional;
    sensitive: bind trash_button.sensitive bidirectional;

    styles ["flat", "trash-button"]

    Box {

      hexpand: true;
      orientation: horizontal;

      Image {
        icon-name: "user-trash-symbolic";
      }

      Label {
        ellipsize: end;
        halign: start;
        label: _("Trash");
      }
    }
  }
}